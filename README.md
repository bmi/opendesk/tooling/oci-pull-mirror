<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
**Content / Quick navigation**

[[_TOC_]]

# Public Git repository to Gitlab pull mirror automation

A fully automated solution to mirror OCI repositories defined in openDesks `images.yaml` and `charts.yaml`.

# Files

The following sub sections describes the contents of the repository beside this `README.md` and the mandatory `LICENSE` information

## `.gitlab-ci.yml`

The CI prepares an Alpine container and executes the script `mirror_automation.py`. All required parameters are provided as CI variables/ENV variables.

## `oci-mirror.py`

The script that takes care of the mirroring. Run `./oci-mirror.py --help` to get information on the available parameters.

Information about the comments required in the `.yaml` files to trigger the mirroring can be found in https://gitlab.opencode.de/bmi/opendesk/deployment/opendesk/-/blob/main/docs/development.md#mirroring

# Trouble shooting

This section will be added one trouble by another.
