#!/usr/bin/python3
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import gitlab
import os
import re
import sys
import yaml
import requests
import logging
import subprocess
import urllib.parse
import configargparse


####################
## INITIALIZATION ##
####################

p = configargparse.ArgParser()
p.add('--gitlab_baseurl', env_var='GITLAB_BASEURL', default='https://gitlab.opencode.de', help='The https base url for the GitLab instance.')
p.add('--gitlab_project_id', env_var='GITLAB_PROJECT_ID', default='1317', help='The ID of the project the yaml files can be found inside.')
p.add('--gitlab_project_accesstoken', env_var='GITLAB_PROJECT_ACCESSTOKEN', required=False, help='If the project is not public provide an access token to access the repos contents: scope "API READ", role "DEVELOPER".')
p.add('--path_images_yaml', env_var='PATH_IMAGES_YAML', required=False, default='helmfile/environments/default/images.yaml.gotmpl', help='Path to the images.yaml.gotmpl within the project/repo, do not prefix with "/".')
p.add('--path_charts_yaml', env_var='PATH_CHARTS_YAML', required=False, default='helmfile/environments/default/charts.yaml.gotmpl', help='Path to the charts.yaml.gotmpl within the project/repo, do not prefix with "/".')
p.add('--gitlab_mirrorbase', env_var='GITLAB_MIRRORBASE', default='bmi/opendesk/components', help='The (url) path to the group the mirror will be created inside.')
p.add('--gitlab_accesstoken', env_var='GITLAB_ACCESSTOKEN', required=True, help='The accesstoken to the mirrorbase, scope "API", "WRITE_REGISTRY" and accesslevel "OWNER" required.')
p.add('--index_branch', env_var='INDEX_BRANCH', default='develop', help='The name of the branch the `images.yaml.gotmpl` and `charts.yaml.gotmpl` are looked up in.')
p.add('--mirror_visibility', env_var='MIRROR_VISIBILITY', default='public', help='GitLab visibility setting (public, internal, private) for script created mirror groups and projects.')
p.add('--loglevel', env_var='LOGLEVEL', default='WARNING', help='Set the loglevel: DEBUG, INFO, WARNING, ERROR, CRITICAL')
options = p.parse_args()

logging.basicConfig(format='%(asctime)s %(message)s', level=options.loglevel)
logging.debug(options)

# Set the required variables, we might want to parametrize these in the future too
artefact_extensions = 'sbom', 'att', 'sig'
crane_platform_parameter = '--platform linux/amd64'
global_visibility = options.mirror_visibility
authenticated_registries = {}
images_yaml_url = f"{options.gitlab_baseurl}/api/v4/projects/{options.gitlab_project_id}/repository/files/{urllib.parse.quote_plus(options.path_images_yaml)}/raw?ref={options.index_branch}"
charts_yaml_url = f"{options.gitlab_baseurl}/api/v4/projects/{options.gitlab_project_id}/repository/files/{urllib.parse.quote_plus(options.path_charts_yaml)}/raw?ref={options.index_branch}"

# Function: Get (yaml) and parse image and charts config files
def load_resource_yaml(url):
    logging.info(f"Getting {url}")
    headers = {}
    if options.gitlab_project_accesstoken:
        headers= {"PRIVATE-TOKEN": options.gitlab_project_accesstoken}
    response = requests.get(url, allow_redirects=True, headers=headers)
    if response.status_code != 200:
        if response.status_code == 404:
            logging.error(f"Got HTTP/404 - If it is not a public repo you probably forgot the --gitlab_project_accesstoken parameter.")
        elif response.status_code == 401:
            logging.error(f"Got HTTP/401 - The provided token may not grant permission to access the given repo.")
        sys.exit(f"! Unable to fetch index file from {url}.")
    # remove comment characters as they are followed by relevant yaml attribute/values for the script
    yaml_config = re.sub(r"( +)# (.+): (.+)",r"\1\2: \3", response.content.decode("utf-8"))
    logging.debug(yaml_config)
    return yaml.safe_load(yaml_config)

# # Function: Do CLI calls
def cli_runner(cli_command):
    logging.info(f"CLI command: {cli_command}")
    try:
        output = subprocess.check_output(cli_command, shell = True)
    except subprocess.CalledProcessError:
        sys.exit(f"! CLI command '{cli_command}' failed with: '{output}'")

def get_tags(registry_details, package_path):
    uri = f"{registry_details}/{package_path}"
    command = f"crane ls {uri}"
    try:
        output = subprocess.check_output(command, shell=True)
        tags = output.decode("utf-8").split("\n")
        logging.debug(f"Got {len(tags)} tags for {uri}")
        return tags
    except subprocess.CalledProcessError:
        logging.warning(f"Unable to fetch tags with {command}")
        return []

def get_digest(artefact):
    command = f"crane {crane_platform_parameter} digest {artefact}"
    try:
        output = subprocess.check_output(command, shell=True)
        digest = output.decode("utf-8").split("\n")[0]
        logging.debug(f"Got digest {digest} for {artefact}")
        return digest
    except subprocess.CalledProcessError:
        logging.warning(f"Unable to fetch digest with {command}")
        return None

# Check if a tag based on the given digest name exists using a defined list of extensions and
# normalizing the digest-string, e.g.
# - sha256:5f44022eab9198d75939d9eaa5341bc077eca16fa51d4ef32d33f1bd4c8cbe7d
# - sha256-7941f6ef6ab3dd691e135233bdb165b255ae77bf635003f3e9b7d06df028cbd0.sig
def copy_related_artefacts(artefact_extensions, source_base, source_digest, source_tags, target_tags, target_base):
    normalized_digest = '-'.join(source_digest.split(':'))
    for ext in artefact_extensions:
        tag_ext = normalized_digest+'.'+ext
        if tag_ext in source_tags:
            logging.debug(f"Found matching artefact: {tag_ext}")
            if tag_ext not in target_tags:
                source_tagged = source_base+':'+tag_ext
                target_tagged = target_base+':'+tag_ext
                logging.debug(f"Mirroring artefact {source_tagged}")
                cli_runner(f"crane copy {source_tagged} {target_tagged}")

# Function: Recursively create groups for a given group_path and return namespace object
def check_and_create_group_structure(group_path, namespace = None):
    group_stack = group_path.split('/')
    logging.info(f"Group hierarchy: {group_stack}")
    try:
        namespace = gl.namespaces.get(group_path)
        return namespace
    except gitlab.exceptions.GitlabGetError as e:
        logging.debug(f"Group autocreate: group not found {group_path} - depth: {len(group_stack)}")
        if len(group_stack) == 1:
            sys.exit(f"Root group {group_path} must exists, please create it manually. If it already exists the provided access_token might not be authorized to access the group.")
        my_group = group_stack[-1]
        my_group_base_stack = group_stack[ : -1]
        my_group_base = '/'.join(my_group_base_stack)
        base_namespace = check_and_create_group_structure(my_group_base, namespace)
        logging.info(f"Creating {my_group} in {my_group_base}")
        parent_group = gl.groups.get(base_namespace.id)
        gl.groups.create({'name': my_group, 'path': my_group, 'parent_id': parent_group.id, 'visibility': global_visibility});
        namespace = gl.namespaces.get(group_path)
        return namespace

def create_registry_project(base_namespace_id, project_name):
    group = gl.groups.get(base_namespace_id)
    projects = group.projects.list(get_all=True)
    project_id = None

    for project in projects:
        if project.path == project_name:
            project_id = project.id

    if not project_id:
        logging.info(f"Creating new project {project_name}")
        project = gl.projects.create({'name': project_name, 'path': project_name, 'namespace_id': base_namespace_id, 'initialize_with_readme': False})
    else:
        project = gl.projects.get(project_id)

    project.visibility = global_visibility
    # Hardcoded project configuration
    project.analytics_access_level = 'disabled'
    project.auto_devops_enabled = False
    project.builds_access_level = 'disabled'
    project.can_create_merge_request_in = False
    project.container_registry_access_level = 'enabled'
    project.container_registry_enabled = True
    project.description = f"Registry mirror"
    project.emails_enabled = False
    project.environments_access_level = 'disabled'
    project.feature_flags_access_level = 'disabled'
    project.forking_access_level = 'disabled'
    project.infrastructure_access_level = 'disabled'
    project.issues_access_level = 'disabled'
    project.issues_enabled = False
    project.jobs_enabled = False
    project.lfs_enabled = False
    project.merge_requests_access_level = 'disabled'
    project.merge_requests_enabled = False
    project.model_experiments_access_level = 'disabled'
    project.monitor_access_level = 'disabled'
    project.package_registry_access_level = 'disabled'
    project.packages_enabled = False
    project.pages_access_level = 'disabled'
    project.releases_access_level = 'disabled'
    project.repository_access_level = 'enabled'
    project.request_access_enabled = False
    project.requirements_enabled = False
    project.security_and_compliance_access_level = 'disabled'
    project.security_and_compliance_enabled = False
    project.service_desk_enabled = False
    project.snippets_access_level = 'disabled'
    project.snippets_enabled = False
    project.wiki_access_level = 'disabled'
    project.wiki_enabled = False
    project.save()
    logging.info(f"Updated project settings for {project_name}")

def authenticate_registry(registry, username, password):
    logging.info(f"Logging into registry {registry}")
    cli_runner(f"crane auth login {registry} -u {username} -p {password}")
    authenticated_registries[registry] = True

sources = {
    'Images': load_resource_yaml(images_yaml_url),
    'Charts': load_resource_yaml(charts_yaml_url)
}
gl = gitlab.Gitlab(url=options.gitlab_baseurl, private_token=options.gitlab_accesstoken)

authenticate_registry('registry.opencode.de', 'oci-mirror', options.gitlab_accesstoken)

filter_re_attr_name = 'upstreamMirrorTagFilterRegEx'
filter_start_attr_name = 'upstreamMirrorStartFrom'

for artefact_class, artefact_config in sources.items():
    for key, regconf in artefact_config[artefact_class.lower()].items():
        logging.info(f"Processing {artefact_class}/{key}")
        if not filter_re_attr_name in regconf:
            logging.warning(f"No {filter_re_attr_name} defined for {artefact_class}/{key}, skipping...")
            continue
        else:
            filter = re.compile(regconf[filter_re_attr_name])
        if not 'upstreamRegistry' in regconf:
            logging.error(f"No upstreamRegistry found for {key}")
            continue
        elif 'opencode.de' in regconf['upstreamRegistry']:
            logging.debug(f"Skipping {key} as it is already on opencode: {regconf['upstreamRegistry']}")
            continue
        else:
            upstream_registry_fqdn = regconf['upstreamRegistry'].removeprefix('https://')
            if 'upstreamRegistryCredentialId' in regconf:
                credential_id = regconf['upstreamRegistryCredentialId']
                logging.debug(f"Found CrendentialId {credential_id} for {upstream_registry_fqdn}")
                if upstream_registry_fqdn not in authenticated_registries:
                    env_name_user = f"MIRROR_CREDENTIALS_SRC_{credential_id}_USERNAME"
                    env_name_pass = f"MIRROR_CREDENTIALS_SRC_{credential_id}_PASSWORD"
                    reg_user = os.environ.get(env_name_user)
                    reg_pass = os.environ.get(env_name_pass)
                    if reg_user and reg_pass:
                        authenticate_registry(upstream_registry_fqdn, env_name_user, env_name_pass)
                    else:
                        logging.error(f"One or both of required env vars not set: {env_name_user}, {env_name_pass}")
                        sys.exit(1)
                else:
                    logging.debug(f"Registry was already authenticated before.")

        # Ensure base group is available
        provider_category = regconf['providerCategory']
        target_group = options.gitlab_mirrorbase+'/'+provider_category
        if provider_category == 'Supplier':
            if not 'providerResponsible' in regconf:
                sys.exit(f"'providerResponsible' definition missing for {key}")
            target_group += '/'+regconf['providerResponsible']
        namespace = check_and_create_group_structure(target_group)
        project_name = f"{artefact_class}-Mirror"
        create_registry_project(namespace.id, project_name)
        target_registry_name = target_group+'/'+project_name+'/'+regconf['upstreamRepository'].split('/')[-1]
        target_tags = get_tags('registry.opencode.de', target_registry_name.lower())
        source_tags = get_tags(upstream_registry_fqdn, regconf['upstreamRepository'])
        target_base = 'registry.opencode.de/'+target_registry_name.lower()
        source_base = upstream_registry_fqdn+'/'+regconf['upstreamRepository']
        for tag in source_tags:
            matches = filter.match(tag)
            if matches:
                logging.debug(f"Tag matches filter regex {tag}")
                if filter_start_attr_name in regconf:
                    if not len(matches.groups()) == len(regconf[filter_start_attr_name]):
                        logging.warning(f"Skipping the filter as the number of match-groups {len(matches.groups())-1} does not equal the number of given filters: {regconf[filter_start_attr_name]}")
                        continue
                    else:
                        index = 1
                        skip = False
                        for from_filter_value in regconf[filter_start_attr_name]:
                            matched_value = int(matches.group(index))
                            from_filter_value = int(from_filter_value)
                            if matched_value > from_filter_value:
                                logging.debug(f"greater than comparison: {matched_value} > {from_filter_value} : true")
                                break
                            if matched_value == from_filter_value:
                                logging.debug(f"equals comparison: {matched_value} = {from_filter_value} : true")
                            else:
                                logging.debug(f"greater than comparison: {matched_value} >= {from_filter_value} : false, skipping this tag")
                                skip = True
                                break
                            index += 1
                        if skip:
                            continue
                source_tagged = source_base+':'+tag
                target_tagged = target_base+':'+tag
                if not tag in target_tags:
                    logging.warning(f"mirroring {tag} for {artefact_class}/{key}")
                    cli_runner(f"crane {crane_platform_parameter} copy {source_tagged} {target_tagged}")
                    source_digest = get_digest(source_tagged)
                    target_digest = get_digest(target_tagged)
                    if (source_digest != target_digest):
                        sys.exit(f"Source digest {source_digest} does not match target digest {target_digest}")
                    else:
                        copy_related_artefacts(artefact_extensions, source_base, source_digest, source_tags, target_tags, target_base)
                else:
                    logging.debug(f"Tag {tag} is available on target")
            else:
                logging.debug(f"Skipping tag '{tag}' as it does not match the filter {filter}")
